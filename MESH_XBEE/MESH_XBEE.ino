#include <TimerOne.h>

/*

+-------------------------------+
| CONFIGURACION DE PINES ROUTER |
+-------------------------------+

Output  PIN  NAME  AT_COMAND bitDigMask ACTUADOR
1       17   DIO3  D3        3          ZONA1 A
2       11   DIO4  D4        4          ZONA1 B
3       15   DIO5  D5        5          ZONA2 A
4       7    DIO11 P1        11         ZONA2 B

Input   PIN NAME  AT_COMAND  bitDigMask SENSOR
1       20  DIO0  D0         0          PIR 90
2       19  DIO1  D1         1          PIR 360 - ZONA1
3       18  DIO2  D2         2          PIR 360 - ZONA2
4        4  DIO12 P2         12         MAGNETICO                                 

+----------------------+
| CONFIGURACION ROUTER |
+----------------------+

PullDown:
 PR=0X151F
 PD=0X0000

Frecuencia de toma de datos:
 IR= 0X01F4 (Cuando esta habilitado)

+---------------+
| FRAMES USADOS |
+---------------+

0X92 -> ROUTER A COORDINADOR

+-------------+
| MODO DE USO |
+-------------+

*MANUAL
*AUTOMATICO
*/

const int addr_MAC=8;             //# de Bytes  de addresses MAC de Xbees, Son dependientes :all_addr, v_address
//  ********************************************************************************
//  * Variables a Actualizar cuando se cambie o agreguen nuevos Xbee a la red !!!  *
//  ********************************************************************************   

const int n_xbee=3;               // Numero de Xbees, Son dependientes: all_addr, dbXbee, v_act
const double clock_T=1000000;     // 1000 ms, no bajar de 500ms!!!
const byte all_addr[n_xbee][addr_MAC]={
                                  {0x00,0x13,0xA2,0x00,0x40,0x65,0xB6,0xEB},
                                  {0x00,0x13,0xA2,0x00,0x40,0x8B,0x49,0x8B},
                                  {0x00,0x13,0xA2,0x00,0x40,0x70,0x4B,0x96}
                                  }; 
/*

R1:			(PullDown)
0x00,0x13,0xA2,0x00,0x40,0xF3,0x94,0xE8
R2:			(PullUp ?)
0x00,0x13,0xA2,0x00,0x40,0xA6,0x2C,0xD8
R3:
0x00,0x13,0xA2,0x00,0x40,0xA6,0x2C,0xD4
R4:
0x00,0x13,0xA2,0x00,0x40,0xB5,0xA1,0x98
R5:
0x00,0x13,0xA2,0x00,0x40,0xB5,0xB8,0x36
R6:
0x00,0x13,0xA2,0x00,0x40,0xB5,0xB8,0x2B
R7:
0x00,0x13,0xA2,0x00,0x40,0xBD,0xA0,0xFF
R8:
0x00,0x13,0xA2,0x00,0x40,0x8B,0x49,0x8B
R9:
0x00,0x13,0xA2,0x00,0x40,0x65,0xB6,0xC7
R10:
0x00,0x13,0xA2,0x00,0x40,0x65,0xB6,0xEB
R11:
0x00,0x13,0xA2,0x00,0x40,0x70,0x4B,0x96
R12:
0x00,0x13,0xA2,0x00,0x40,0x92,0x04,0x07
R13:
0x00,0x13,0xA2,0x00,0x40,0x92,0x04,0x2E

*/
//  ********************************************************************************
volatile boolean activate= false; //Permite lectura de Xbees
boolean v_act[n_xbee];            //Matriz de confirmacion para "activate"
boolean wState=true;              //Permite lectura de Xbees, a pesar de existir desconectados
const int BYTE_COUNT=22;          //Frame: 0x92
const int qPinsXB=8;              //Cantidad de pines usados en c/XBee Router
int ReadPins=0;                   //Almacenará trama de datos de Xbee's Pins
volatile byte v_address[addr_MAC];         //Array variable donde se carga la Direccion MAC de Xbee leido
volatile int dbXbee[n_xbee][qPinsXB];      //DataBase de Pins de Xbee                       
int pos=1000;

volatile int dbMySQL[n_xbee][qPinsXB/2];   //DataBase de Outputs de Xbee                       
volatile int v_A_M[n_xbee];                //Vector de estados Manual o Auto de Xbees  
volatile int dbVolat[n_xbee][qPinsXB/2];   //DataBase momentaneo representa los Outputs en Manual y/o Auto   


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Timer1.initialize(clock_T);
  Timer1.attachInterrupt(Stop_Reading) ;  //Timer de parada de lectura forzada
}

void loop() {
  // put your main code here, to run repeatedly:


//Leer Xbee                               ---Update "dbXbee"             *Modificado 11.24
readXbee();
//Leer DataBase MySQL                     ---Update "dbMySQL"            *Modificado 11.24
readMySQL();                  //FALTA
//Funcion de Auto y Manual                ---Update "dbVolat"
fun_A_M();                    //FALTA                                    *Modificado 10.50
//Funcion de escritura en Xbees usando "dbVolat"     
writeXbee();
////Leer Xbee                               ---Update "dbXbee"
//readXbee();
////Funcion de escritura en DataBase MySQL usando "dbXbee"
//writeMySQL();                 //FALTA

}






void readMySQL(){
//------------------------------------------------------------------------------
//Lectura de DataBase MySQL y actualizacion de dbMySQL:


//OJO ELIMINAR:
// dbMySQL[0][0]=1; dbMySQL[0][1]=0; dbMySQL[0][2]=1; dbMySQL[0][3]=0;
// dbMySQL[1][0]=0; dbMySQL[1][1]=0; dbMySQL[1][2]=0; dbMySQL[1][3]=0;
// dbMySQL[2][0]=0; dbMySQL[2][1]=1; dbMySQL[2][2]=0; dbMySQL[2][3]=1;

// dbMySQL[0][0]=0; dbMySQL[0][1]=1; dbMySQL[0][2]=0; dbMySQL[0][3]=1;
// dbMySQL[1][0]=1; dbMySQL[1][1]=1; dbMySQL[1][2]=1; dbMySQL[1][3]=1;
// dbMySQL[2][0]=1; dbMySQL[2][1]=0; dbMySQL[2][2]=1; dbMySQL[2][3]=0;
 
 dbMySQL[0][0]=1; dbMySQL[0][1]=1; dbMySQL[0][2]=1; dbMySQL[0][3]=1;
 dbMySQL[1][0]=1; dbMySQL[1][1]=1; dbMySQL[1][2]=1; dbMySQL[1][3]=1;
 dbMySQL[2][0]=1; dbMySQL[2][1]=1; dbMySQL[2][2]=1; dbMySQL[2][3]=1;
 
 
//------------------------------------------------------------------------------
}

void fun_A_M(){

  //Llama de MySQL xbees con Auto y Manual,
  //cargarlos en array v_M_A

    

//OJO ELIMINAR
//  Modo Manual
//  v_A_M[0]=1;  v_A_M[1]=1;  v_A_M[2]=1;  
// Modo Automatico solo uno
  v_A_M[0]=0;  v_A_M[1]=1;  v_A_M[2]=1;  
  
  //Ej:
  //v_A_M={0,1,0};          0(Auto) 1(Manual)


  
  for(int i = 0; i<n_xbee; i++){
    if (v_A_M[i]==0){   //Es Auto
/*Logica Programable
O1
!I3*I1+I2*I1+I4
O2
!I3*I1+I2*I1+I4
O3
!I2*I1+I3*I1+I4
O4
!I2*I1+I3*I1+I4
*/ 
         dbVolat[i][0]=(~dbXbee[i][2]&dbXbee[i][0])|(dbXbee[i][1]&dbXbee[i][0])|dbXbee[i][3];    //O1
         dbVolat[i][1]=(~dbXbee[i][2]&dbXbee[i][0])|(dbXbee[i][1]&dbXbee[i][0])|dbXbee[i][3];    //O2
         dbVolat[i][2]=(~dbXbee[i][1]&dbXbee[i][0])|(dbXbee[i][2]&dbXbee[i][0])|dbXbee[i][3];    //O3
         dbVolat[i][3]=(~dbXbee[i][1]&dbXbee[i][0])|(dbXbee[i][2]&dbXbee[i][0])|dbXbee[i][3];    //O4
      }
    if (v_A_M[i]==1){   //Es Manual
        for(int j=0; j<qPinsXB/2; j++){
          dbVolat[i][j]=dbMySQL[i][j];
          }      
      }
    }
   
 /*  Comentar solo para Pruebas, Que muestra dbVolat*/

//dbVolat
Serial.print("\ndbVolat\n");
Serial.print(dbXbee[0][0]);Serial.print(dbXbee[0][1]);Serial.print(dbXbee[0][2]);Serial.print(dbXbee[0][3]);Serial.print(dbXbee[0][4]);Serial.print(dbXbee[0][5]);Serial.print(dbXbee[0][6]);Serial.println(dbXbee[0][7]);
Serial.print(dbXbee[1][0]);Serial.print(dbXbee[1][1]);Serial.print(dbXbee[1][2]);Serial.print(dbXbee[1][3]);Serial.print(dbXbee[1][4]);Serial.print(dbXbee[1][5]);Serial.print(dbXbee[1][6]);Serial.println(dbXbee[1][7]);
Serial.print(dbXbee[2][0]);Serial.print(dbXbee[2][1]);Serial.print(dbXbee[2][2]);Serial.print(dbXbee[2][3]);Serial.print(dbXbee[2][4]);Serial.print(dbXbee[2][5]);Serial.print(dbXbee[2][6]);Serial.println(dbXbee[2][7]);

}

void writeXbee(){
  //Funcion de escritura en Xbee usando "dbVolat"
    for(int i = 0; i<n_xbee; i++){
      for(int j = 0 ; j<qPinsXB/2; j++){
        setPinXBEE(i,j+1,dbVolat[i][j]);
      }
    }  
}

void writeMySQL(){
  

  
}

void setPinXBEE(int nXbee, int n_output, int H_L) {
//------------------------------------------------------------------------------
//Escritura de Xbees:

  byte volat;
  Serial.write(0x7E);
  Serial.write(0x00);
  Serial.write(0x10);
  
  Serial.write(0x17);
  Serial.write(0x00);
  long checkS=0x17+0x00;
  
  //Enviando para el MAC del Xbee seleccionado
  for (int i = 0; i<addr_MAC; i++) {  // Almacenar Direccion MAC usada
    volat=all_addr[nXbee][i];
    checkS=checkS+volat;              // + Checksum
    Serial.write(volat);          
  }
  
  Serial.write(0xFF);                 //Address Zigbee unknown
  Serial.write(0xFE);
  
  Serial.write(0x02);
  checkS=checkS+0xFF;
  checkS=checkS+0xFE;
  checkS=checkS+0x02;

  //Seleccionando Pin
  if (n_output == 1){   //D3
    Serial.write(0x44);
    Serial.write(0x33);
    checkS=checkS+0x44+0x33;   
    }
  if (n_output == 2){   //D4
    Serial.write(0x44);
    Serial.write(0x34);
    checkS=checkS+0x44+0x34;       
    }
  if (n_output == 3){   //D5
    Serial.write(0x44);
    Serial.write(0x35);
    checkS=checkS+0x44+0x35;       
    }
  if (n_output == 4){   //P1
    Serial.write(0x50);
    Serial.write(0x31);      
    checkS=checkS+0x50+0x31; 
    }
            
//Seleccionando Estado
  if (H_L == 1){
    volat=0x05;
    checkS=checkS+volat;
    Serial.write(volat);    
    }  
  if (H_L == 0){
    volat=0x04;
    checkS=checkS+volat;
    Serial.write(volat);    
    }
       
//Checksum
  Serial.write(0xFF-(checkS&0xFF));
//------------------------------------------------------------------------------
}

int knownXB(int n_xbee,int addr_MAC,byte byte7,byte byte6,byte byte5,byte byte4,byte byte3,byte byte2,byte byte1,byte byte0){
  int found=0;
  for (int i = 0; i<n_xbee; i++) {  
    if (byte7==all_addr[i][0] && byte6==all_addr[i][1] && byte5==all_addr[i][2] && byte4==all_addr[i][3] && byte3==all_addr[i][4] && byte2==all_addr[i][5] && byte1==all_addr[i][6] && byte0==all_addr[i][7]){
      found=1;
      return i;    
    }
  }    
  if (found == 0){
    return 1000; //DESCONOCIDO REGRESA 1000
    }
}
void Stop_Reading(){
  wState=!wState;
  }
void readXbee(){
//------------------------------------------------------------------------------
//Lectura de Xbees y actualizacion de dbXbee:


  //--------------------------------------------------------------+   
  //Limpiar dbXbee, buffer, wState, v_act y activate, activa timer  
  for(int i = 0; i<n_xbee; i++){
    for(int j = 0; j<qPinsXB; j++){
        dbXbee[i][j]=0;
      }
    }
  Serial.flush();
  for (int i = 0; i<n_xbee; i++) {
    v_act[n_xbee]=false;
  }
  activate=false;
  noInterrupts(); //Desactivar timer
  wState=true;
  interrupts();
  //--------------------------------------------------------------+ 

      
while(!activate && wState){    //Mientras no halla leido a los "n" Xbees esclavos  o
if(Serial.available()>=BYTE_COUNT){
  if (Serial.read() == 0x7E) { // 7E Inicio de FRAME
    
    byte discardByte = Serial.read();
    discardByte = Serial.read();
    discardByte = Serial.read(); // Este tiene que ser 0x92  
      
    for (int i = 0; i<addr_MAC; i++) { // Almacenar Direccion MAC usada
      v_address[i] = Serial.read();
    }
       
    //Si v_address conocido,enviar confirmacion y 
    //Activar escritura en dbXbee y cambiar celda de v_act a "TRUE"

    pos=knownXB(n_xbee,addr_MAC,v_address[0],v_address[1],v_address[2],v_address[3],v_address[4],v_address[5],v_address[6],v_address[7]);
    //Posicion de Xbee en Matriz dbXbee
    
    if (pos != 1000){//Si se encuentra en la Matriz dbXbee
      v_act[pos]=true; //Leyo Xbee de posicion "pos"
      for (int i = 0; i<7; i++) { // Deshechado: Direccion ZigBee, Dig & Anal Mask, others
        byte discardByte = Serial.read();
      }
      //Lectura de estados de Xbee Pines
      int Read1 = Serial.read(); // Lectura "Parte1" de Pines - HEX 
      int Read2 = Serial.read(); // Lectura "Parte2" de Pines - HEX
      ReadPins = (Read1*256) + Read2;
      
      //Escritura en dbXbee
      dbXbee[pos][0]=ReadPins&0x1;                                      //I1=dbXbee[n][0]
      dbXbee[pos][1]=ReadPins&0x2;dbXbee[pos][1]=dbXbee[pos][1]>>1;     //I2=dbXbee[n][1]
      dbXbee[pos][2]=ReadPins&0x4;dbXbee[pos][2]=dbXbee[pos][2]>>2;     //I3=dbXbee[n][2]
      dbXbee[pos][3]=ReadPins&0x1000;dbXbee[pos][3]=dbXbee[pos][3]>>12; //I4=dbXbee[n][3]
      dbXbee[pos][4]=ReadPins&0x8;dbXbee[pos][4]=dbXbee[pos][4]>>3;     //O1=dbXbee[n][4]
      dbXbee[pos][5]=ReadPins&0x10;dbXbee[pos][5]=dbXbee[pos][5]>>4;    //O2=dbXbee[n][5]
      dbXbee[pos][6]=ReadPins&0x20;dbXbee[pos][6]=dbXbee[pos][6]>>5;    //O3=dbXbee[n][6]
      dbXbee[pos][7]=ReadPins&0x800;dbXbee[pos][7]=dbXbee[pos][7]>>11;  //O4=dbXbee[n][7]
  
      ReadPins =0;                      
    }
    else{           //Si no se encuentra en la Matriz dbXbee
      for (int i = 0; i<10; i++) { // Deshechado
        byte discardByte = Serial.read();
      }
       Serial.println("No se encontro Frame");
    }    
   }
}

//activate: Verificara que haya leido a los n Xbees
activate=true;

/*  Comentar, solo para pruebas, si reconocio XBEE y que Xbees

//Serial.print("\n\n");
//Serial.println(v_act[0]);Serial.println(v_act[1]);Serial.println(v_act[2]);Serial.println(activate);
//Serial.print("\n\n");
*/


for (int i = 0; i<n_xbee; i++) {
activate=activate&&v_act[i];
}


}

/*  Comentar solo para Pruebas, Que muestra dbXbee*/

//dbXbee
Serial.print("\ndbXbee\n");
Serial.print(dbXbee[0][0]);Serial.print(dbXbee[0][1]);Serial.print(dbXbee[0][2]);Serial.print(dbXbee[0][3]);Serial.print(dbXbee[0][4]);Serial.print(dbXbee[0][5]);Serial.print(dbXbee[0][6]);Serial.println(dbXbee[0][7]);
Serial.print(dbXbee[1][0]);Serial.print(dbXbee[1][1]);Serial.print(dbXbee[1][2]);Serial.print(dbXbee[1][3]);Serial.print(dbXbee[1][4]);Serial.print(dbXbee[1][5]);Serial.print(dbXbee[1][6]);Serial.println(dbXbee[1][7]);
Serial.print(dbXbee[2][0]);Serial.print(dbXbee[2][1]);Serial.print(dbXbee[2][2]);Serial.print(dbXbee[2][3]);Serial.print(dbXbee[2][4]);Serial.print(dbXbee[2][5]);Serial.print(dbXbee[2][6]);Serial.println(dbXbee[2][7]);

//------------------------------------------------------------------------------
}


